const Order = require('../models/mdl_order');
const Product = require('../models/mdl_product');
const User = require('../models/mdl_user');
const Cart = require('../models/mdl_cart');
const auth = require('../auth');

// Controller for getting a user's cart
module.exports.getMyCart = async (reqBody) => {
	let productId = reqBody.productId;
	let userId = reqBody.userId;
	let productName = reqBody.productName;
	let price = reqBody.price;
	let quantity = reqBody.quantity;
	let subTotal = quantity * price;
	let total = subTotal;
	let newCart = new Cart ({
		userId: userId,
		total: total,
		cartItems : {
			productId: productId,
			productName: productName,
			price: price,
			quantity: quantity,
			subTotal: subTotal

		}

	})
	console.log(newCart)
	return newCart.save().then((result, error) =>{
		if (error) {
			return false;
		}
		else{
			return true;
		}
	})
	.catch(err => {
		console.log(err); // Remember to handle error to avoid hanging the request
		return false;
	});
}
