const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const userRoutes = require("./routes/rt_user");
const productRoutes = require("./routes/rt_product");
const orderRoutes = require("./routes/rt_order")
const cartRoutes = require("./routes/rt_cart")
const env = require('dotenv/config');



// Connect to MongoDB
mongoose.connect(process.env.DB,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
);

app.use(
	cors({
		origin: "*",
		methods: ["GET", "POST", "PUT"]
	})
)

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'));

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes);

// Defines the "/products" string to be included for all user routes defined in the "product" route file
app.use("/products", productRoutes);

// Defines the "/orders" string to be included for all user routes defined in the "order" route file
app.use("/orders", orderRoutes);

// Defines the "/carts" string to be included for all user routes defined in the "order" route file
app.use("/carts", cartRoutes);

// Listening to port
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }.`)
});