const mongoose = require("mongoose");
const cartSchema = new mongoose.Schema({
	userId : {
		type : String,
		required : [true, "User ID is required."]
	},

	cartItems: [{
		productId : {
			type: String,
			required:[true, "Product ID is required."]
		},
		productName : {
			type: String,
			required:[true, "Product name is required."]
		},
		price : {
			type: String,
			required:[true, "Price is required."]
		},
		subTotal : {
			type: Number,
			required: [true, "Subtotal is required."]
		}
	}],
	total: {
		type: Number
	}
});

module.exports = mongoose.model("Cart", cartSchema);
