const express = require ("express");
const auth = require("../auth");
const cartController = require('../controllers/ctrl_cart');
const router = express.Router();



// Route for getting cart
router.post("/myCart", async (req, res) => {
	cartController.getMyCart(req.body).then(result => res.send(result));
})
module.exports = router;